import dotenv from 'dotenv';
dotenv.config();

class CacheConfigService {
  constructor(env) {
    this.env = env;
  }

  /**
   * @param key {string}
   * @param throwOnMissing {boolean}
   * @return {*}
   */
  getValue(key, throwOnMissing = true) {
    const value = this.env[key];
    if (!value && throwOnMissing) {
      throw new Error(`config error - missing env.${key}`);
    }

    return value;
  }

  /**
   * @param keys {string[]}
   * @return {CacheConfigService}
   */
  ensureValues(keys) {
    keys.forEach(k => this.getValue(k, true));
    return this;
  }

  getCacheConnectionUrl() {
    return { url: `redis://${this.getValue("REDIS_HOST")}:${this.getValue("REDIS_PORT")}` };
  }

  getKeyExpiration() {
    return Number(this.getValue("CACHE_LIVE_TIME", false)) || 300;
  }

  isKeyHashingEnable() {
    return this.getValue("CACHE_MODE", false) === 'hash';
  }
}

export const cacheConfigProvider = new CacheConfigService(process.env)
  .ensureValues([
    "REDIS_HOST",
    "REDIS_PORT",
    "CACHE_LIVE_TIME",
  ]);
