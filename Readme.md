## Redis cache with tags support

### When creating cache instance requires object of keys and tags that will be used in the application

#### Keys object example
    Keys = {
        key1: value1,
        key2: value2,
        key3: value3,
    }

#### Tags object example
     Tags = { 
        key1: value1, 
        key2: value2,
        key3: { 
            name: value3
            key4: value4, 
            key5: value5, 
            key6: { 
                name: value6,
                key7: value7
            } 
        } 
     }
