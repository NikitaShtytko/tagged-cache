import { createClient } from 'redis';
import { cacheConfigProvider } from "./config.js";
import crypto from "crypto";

let cacheInstance;

/**
 * @param keys {object}
 * @param tags {object}
 */
export function getCacheInstance(keys, tags) {
  if (!cacheInstance) {
    cacheInstance = new Provider(keys, tags);
  }

  return cacheInstance;
}

export class Provider {
  /**
   * @param keys {object}
   * @param tags {object}
   */
  constructor(keys, tags) {
    let client
    client = createClient(cacheConfigProvider.getCacheConnectionUrl());

    client.on('error', async function (err) {
      console.log('Redis init error' + err);
    });

    this.cache = client;
    this.keys = keys;
    this.tags = tags;
    this.keyExpireTime = cacheConfigProvider.getKeyExpiration()
    this.isHashMode = cacheConfigProvider.isKeyHashingEnable();
  }

  async connect() {
    this.cache.connect();
  }

  async disconnect() {
    this.cache.disconnect();
  }

  getKeyExpireConfig() {
    return { EX: this.keyExpireTime };
  }

  /**
   * @param value {string}
   * @return {string}
   */
  getHashedValue(value) {
    if (!this.isHashMode)
      return value;

    return crypto.createHash('md5').update(value).digest("hex");
  }

  /**
   * @param key {string}
   * @return {string}
   */
  getHashedCacheKey(key) {
    if (this.keys && !this.keys.includes(key)) {
      throw new Error(`Key ${key} is not presented in the list`)
    }

    return this.getHashedValue(key);
  }

  /**
   * @param value {string}
   * @param tags {object}
   * @return {string[]}
   */
  findTagsPosition(value, tags = this.tags) {
    for (let key in tags) {
      if (tags[key] && typeof tags[key] === "object") {
        let result = this.findTagsPosition(value, tags[key]);
        if (result) {
          result.unshift(key);
          return result;
        }
      } else if (tags[key] === value) {
        return [tags[key]];
      }
    }
  }


  /**
   * @param object {object | string | number}
   * @return {*[]}
   */
  findRoot(object) {
    if (typeof object !== 'object')
      return [object];

    const array = [];
    for (let key in object) {
      if (object[key] && typeof object[key] === 'object') {
        let result = this.findRoot(object[key]);
        if (result) {
          array.push(...result);
          return array;
        }
      } else {
        array.push(object[key]);
      }
    }

    return array;
  }

  /**
   * @param value
   * @param tags
   * @return {string[] | undefined}
   */
  findTagRoots(value, tags = this.tags) {
    const positions = this.findTagsPosition(value);

    let originObject = tags;
    positions.map(item => {
      if (Object.keys(originObject).includes(item) && item !== 'name') {
        originObject = originObject[item];
      }
    });

    return this.findRoot(originObject);
  }

  /**
   * @param value
   * @return {string[] | undefined}
   */
  findTagPath(value) {
    const positions = this.findTagsPosition(value);
    if (!positions) return;

    const result = [];
    const lastElementIndex = positions.length - 1;
    let object = this.tags;
    for (let key in positions) {
      if (key < lastElementIndex) {
        object = object[positions[key]];
        result.push(object.name)
      }
    }

    if (!result.includes(positions[lastElementIndex]))
      result.push(positions[lastElementIndex]);

    return result;
  }

  /**
   * @param tag {string}
   * @param findRootTags {boolean}
   * @return {string[]}
   */
  getHashedCacheTags(tag, findRootTags = false) {
    if (!this.tags)
      return [];

    let tagKeys = this.findTagPath(tag);
    if (!tagKeys) {
      throw new Error(`Tag ${tag} is not presented in the list`)
    }

    if (findRootTags) {
      tagKeys = this.findTagRoots(tag);
    }

    tagKeys.map((item, index) => {
      tagKeys[index] = this.getHashedValue(tagKeys[index]);
    });

    return tagKeys;
  }

  /**
   * @param key {string}
   * @param tag {string}
   * @return {{cacheKey: string, tags: string[]}}
   */
  getHashedCacheKeyAndTags(key, tag) {
    const cacheKey = this.getHashedCacheKey(key);
    const tags = this.getHashedCacheTags(tag);

    return { cacheKey, tags };
  }

  /**
   * @param key {string}
   * @param tag {string}
   * @param callback {function}
   * @return {Promise<{cacheKey: string, tag: string}>}
   */
  async getOrSet(key, tag, callback) {
    const { cacheKey, tags } = this.getHashedCacheKeyAndTags(key, tag);

    const cacheValue = await this.cache.get(cacheKey);
    if (cacheValue) return JSON.parse(cacheValue);

    const result = await callback();
    await this.cache.set(cacheKey, JSON.stringify(result), this.getKeyExpireConfig());

    await this.updateCacheTagsValues(tags, cacheKey);

    return result;
  }

  async getCacheByKey(key){
    const cacheKey = this.getHashedCacheKey(key);

    return await this.cache.get(cacheKey);
  }

  /**
   * @param cacheTags {string[]}
   * @param key {string}
   */
  async updateCacheTagsValues(cacheTags, key) {
    await Promise.all(cacheTags.map(async (item, index) => {
      if (cacheTags[index + 1])
        await this.cache.sAdd(cacheTags[index], cacheTags[index + 1])
      else
        await this.cache.sAdd(cacheTags[index], key)
    }));
  }

  /**
   * @param tag {string}
   * @return {Promise<void>}
   */
  async deleteCacheByExactTag(tag) {
    const cacheKeysByTag = await this.cache.sMembers(tag);
    if (cacheKeysByTag.length) {
      await this.cache.sRem(tag, cacheKeysByTag);
      await this.cache.del(cacheKeysByTag);
    }
  }

  /**
   * @param tag {string}
   * @return Promise<void>
   */
  async deleteCacheByTag(tag) {
    const cacheTags = this.getHashedCacheTags(tag, true).reverse();

    await Promise.all(cacheTags.map(async tag => await this.deleteCacheByExactTag(tag)));
  }

  /**
   * @param key {string}
   * @return Promise<void>
   */
  async deleteCacheByKey(key) {
    const cacheKey = this.getHashedCacheKey(key);
    await this.cache.del(cacheKey);
  }
}